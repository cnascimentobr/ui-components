/*
* Document: util.js
* Util JavaScript
* Author: Christian Nascimento
*/
var App = window.App || (window.App = {});
App.utilModule = (function($){
  "use strict";
  var handleFileSelect = function(evt) {
    var files = evt.target.files; // FileList object

    // Loop through the FileList and render image files as thumbnails.
    for (var i = 0, f; f = files[i]; i++) {

      // Only process image files.
      if (!f.type.match('image.*')) {
        continue;
      }

      var reader = new FileReader();

      // Closure to capture the file information.
      reader.onload = (function(theFile) {
        return function(e) {
          // Render thumbnail.
          var span = document.createElement('span');
          span.innerHTML = ['<img class="thumb" src="', e.target.result,
                            '" title="', escape(theFile.name), '"/>'].join('');
          $(".thumbnail").html(span);
        };
      })(f);

      // Read in the image file as a data URL.
      reader.readAsDataURL(f);
    }
  };
  var data = {
    set: function(key, value) {
      if (!key || !value) {return;}

      if (typeof value === "object") {
        value = JSON.stringify(value);
      }
      localStorage.setItem(key, value);
    },
    get: function(key) {
      var value = localStorage.getItem(key);

      if (!value) {return;}

      // assume it is an object that has been stringified
      if (value[0] === "{") {
        value = JSON.parse(value);
      }

      return value;
    }
  };
  var getAgeMsg = function(n){
    var num = Number(n);
    switch(num) {
      case 0:
        return "above 13 years";
        break;
      case 1:
        return "above 20 years";
        break;
      case 2:
        return "above 30 years";
        break;
      case 3:
        return "above 45 years";
        break;
    }
  },
  getAddress = function(a){
    switch(a) {
      case "Home":
        $(".home").show();
        $(".company").hide();
        return {type:"home","address1": $("#address1").val(), "address2": $("#address2").val()};
        break;
      case "Company":
        $(".home").hide();
        $(".company").show();
        return {type:"company","company1": $("#company-address1").val(), "company2": $("#company-address2").val()};
        break;
    }
  };
  return {
    data: data,
    handleFileSelect: handleFileSelect,
    getAddress: getAddress,
    getAgeMsg: getAgeMsg
  };
})(jQuery);
