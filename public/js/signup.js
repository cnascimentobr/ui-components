/*
* Document: signup.js
* Signup JavaScript
* Author: Christian Nascimento
*/
var App = window.App || (window.App = {});
App.signupModule = (function($){
  "use strict";
  var $signup = $(".signup");
  var ready = function(){
    $(".btn-upload-photo").click(function(){
      $("#photo").click();
    });
    $("#photo").on('change', function(e){
      App.utilModule.handleFileSelect(e);
    });
    $("#address").change(function(){
      var a = this.value;
      switch(a) {
        case "Home":
          $(".home-address").show();
          $(".company-address").hide();
          break;
        case "Company":
          $(".home-address").hide();
          $(".company-address").show();
          break;
        default:
          $(".home-address").hide();
          $(".company-address").hide();
      }
    });
    $signup.submit(function(e){
      e.preventDefault();
      window.localStorage.clear();
      var objData = {
        firstName: $("#first-name").val(),
        lastName: $("#last-name").val(),
        age: $("#age").val(),
        email: $("#email").val(),
        tel: $("#tel").val(),
        state: $("#state").val(),
        country: $("#country").val(),
        address: App.utilModule.getAddress($("#address").val()),
        interests: $("#interests").val(),
        newsletter: $("#newsletter").is(":checked"),
        image: $(".thumb").attr("src")
      };
      App.utilModule.data.set("profile", objData);
      window.location.href = "/profile";
    });
  };
  return{
    ready: ready
  }
})(jQuery);
$(document).on("ready", App.signupModule.ready);
