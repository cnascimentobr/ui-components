/*
* Document: main.js
* Default JavaScript
* Author: Christian Nascimento
*/
var App = {};
window.App || (window.App = {});
App.init = (function($){
  var ready = function(){
    
  };
  return{
    ready: ready
  }
})(jQuery);
$(document).on("ready", App.init.ready);
