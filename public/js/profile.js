/*
* Document: profile.js
* Validation JavaScript
* Author: Christian Nascimento
*/
var App = window.App || (window.App = {});
App.profileModule = (function($){
  "use strict";
  var profile = {
    data: App.utilModule.data.get("profile"),
    info: function(){
      var info = "I am " + this.data.firstName + " " + this.data.lastName;
          info += " and I am above " + App.utilModule.getAgeMsg(this.data.age) + " and you can send your email to ";
          info += this.data.email + ". I live in states of " + this.data.state + ".";
          info += " My interests are: " + this.data.interests + ".";
          info += this.data.newsletter == true ? " And please send me the newsletter." : "";
          info += " Please reach out to me on my phone " + this.data.tel;
      return info;
    }
  }
  var init = function(){
    $(".thumbnail").html("<img src=" + profile.data.image + ">");
    $(".info").html("<p>" + profile.info() + "</p>");
    $("#photo").on('change', function(e){
      App.utilModule.handleFileSelect(e);
      setTimeout(function(){
        window.localStorage.clear();
        var objData = {
          firstName: profile.data.firstName,
          lastName: profile.data.lastName,
          age: profile.data.age,
          email: profile.data.email,
          tel: profile.data.tel,
          state: profile.data.state,
          country: profile.data.country,
          address: profile.data.address,
          interests: profile.data.interests,
          newsletter: profile.data.newsletter,
          image: $(".thumb").attr("src")
        };
        App.utilModule.data.set("profile", objData);
      },2000);
    });
    $(".edit-photo").on("click", function(){
      $("#photo").click();
    })
  }
  return {
    init: init
  };
})(jQuery);
$(document).on("ready", App.profileModule.init);
