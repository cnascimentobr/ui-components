/*
* Document: edit.js
* Signup JavaScript
* Author: Christian Nascimento
*/
var App = window.App || (window.App = {});
App.editModule = (function($){
  "use strict";
  var profile = {
    data: App.utilModule.data.get("profile"),
    getAgeValue: function(){

    },
    loadValues: function(){
      var addresType = (profile.data.address.type).replace(/^[a-z]/, function(m){ return m.toUpperCase() });
      $("#first-name").val(profile.data.firstName);
      $("#last-name").val(profile.data.lastName);
      $("#age").val(profile.data.age);
      $("#email").val(profile.data.email);
      $("#tel").val(profile.data.tel);
      $("#state").val(profile.data.state);
      $("#country").val(profile.data.country);
      $("#address").val(addresType);
      setTimeout(function(){
        $("#address").change();
        if(addresType == 'Home'){
          $("#address1").val(profile.data.address.address1);
          $("#address2").val(profile.data.address.address2);
        }else{
          $("#company-address1").val(profile.data.address.company1);
          $("#company-address2").val(profile.data.address.company2);
        }
      });
      $("#interests").val(profile.data.interests);
      $("#newsletter").attr("checked", profile.data.newsletter);
      var span = document.createElement('span');
      span.innerHTML = ['<img class="thumb" src="', profile.data.image,
                        '"/>'].join('');
      $(".thumbnail").html(span);
    }
  }
  var $edit = $(".edit");
  var ready = function(){
    profile.loadValues();
    $(".btn-upload-photo").click(function(){
      $("#photo").click();
    });
    $("#photo").on('change', function(e){
      App.utilModule.handleFileSelect(e);
    });
    $("#address").change(function(){
      var a = this.value;
      switch(a) {
        case "Home":
          $(".home-address").show();
          $(".company-address").hide();
          break;
        case "Company":
          $(".home-address").hide();
          $(".company-address").show();
          break;
        default:
          $(".home-address").hide();
          $(".company-address").hide();
      }
    });
    $edit.submit(function(e){
      e.preventDefault();
      var objData = {
        firstName: $("#first-name").val(),
        lastName: $("#last-name").val(),
        age: $("#age").val(),
        email: $("#email").val(),
        tel: $("#tel").val(),
        state: $("#state").val(),
        country: $("#country").val(),
        address: App.utilModule.getAddress($("#address").val()),
        interests: $("#interests").val(),
        newsletter: $("#newsletter").is(":checked"),
        image: $(".thumb").attr("src")
      };
      App.utilModule.data.set("profile", objData);
      window.location.href = "/profile";
    });
  };
  return{
    ready: ready
  }
})(jQuery);
$(document).on("ready", App.editModule.ready);
