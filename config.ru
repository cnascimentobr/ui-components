use Rack::Static,
  :urls => ["/images", "/js", "/css"],
  :root => "public"

  map "/" do
    run lambda { |env|
    [
      200,
      {
        'Content-Type'  => 'text/html',
        'Cache-Control' => 'public, max-age=86400'
      },
      File.open('public/index.html', File::RDONLY)
    ]
  }
  end

  map "/signup" do
    run lambda { |env|
    [
      200,
      {
        'Content-Type'  => 'text/html',
        'Cache-Control' => 'public, max-age=86400'
      },
      File.open('public/signup.html', File::RDONLY)
    ]
  }
  end

  map "/profile" do
    run lambda { |env|
    [
      200,
      {
        'Content-Type'  => 'text/html',
        'Cache-Control' => 'public, max-age=86400'
      },
      File.open('public/profile.html', File::RDONLY)
    ]
  }
  end

  map "/edit" do
    run lambda { |env|
    [
      200,
      {
        'Content-Type'  => 'text/html',
        'Cache-Control' => 'public, max-age=86400'
      },
      File.open('public/edit.html', File::RDONLY)
    ]
  }
  end
