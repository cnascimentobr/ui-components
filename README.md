# UI-Components exercise#

This repository is a ui-component exercise that fills a profile based in a registration page form.

I used `localStorage` to set and load the profile data.

I used HTML5 api to upload profile image.

### What I used? ###

* Bootstrap
* JavaScript
* HTML

## How to test ##

### Online ###
I deployed a version to heroku and you can access with the url: https://stormy-beyond-6767.herokuapp.com/